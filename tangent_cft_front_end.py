import argparse

from Embedding_Preprocessing.encoder_tuple_level import TupleTokenizationMode
from tangent_cft_back_end import TangentCFTBackEnd


def main():
    parser = argparse.ArgumentParser(description='Given the configuration file for training Tangent_CFT model.'
                                                 'This function train the model and then does the retrieval task on'
                                                 'NTCIR-12 formula retrieval task.')

    parser.add_argument('-ds', type=str, help="File path of training data. If using NTCIR12 dataset, "
                                              "it should be MathTagArticles directory. If using the MSE dataset, it"
                                              "should be csv file of formula", required=True)
    parser.add_argument('-cid', metavar='cid', type=int, help='Configuration file.', required=True)
    parser.add_argument('-em', type=str, help="File path for encoder map.", required=True)
    parser.add_argument('--mp', type=str, help="Model file path.", default=None)
    parser.add_argument('--qd', type=str, help="NTCIR12 query directory.", default=None)
    parser.add_argument('--rf', type=str, help="Retrieval result file path.", default="ret_res")
    parser.add_argument('--ri', type=int, help="Run Id for Retrieval.", default=1)
    parser.add_argument('--et', help='Embedding type; 1:Value, 2:Type, 3:Type and Value separated and'
                                     ' 4: Type and Value Not Separated', choices=range(1, 5),
                        default=3, type=int)

    feature_parser = parser.add_mutually_exclusive_group(required=False)
    feature_parser.add_argument('--train', dest='train', action='store_true', help="Train a model")
    feature_parser.add_argument('--ntrain', dest='train', action='store_false', help="Not train a new model")
    parser.set_defaults(train=True)

    feature_parser = parser.add_mutually_exclusive_group(required=False)
    feature_parser.add_argument('--ret', dest='retrieval', action='store_true', help="Retrieve formulae for the queries")
    feature_parser.add_argument('--nret', dest='retrieval', action='store_false', help="No retrieval.")
    parser.set_defaults(retrieval=True)


    feature_parser = parser.add_mutually_exclusive_group(required=False)
    feature_parser.add_argument('--slt', dest='read_slt', action='store_true', help="Use SLT representation")
    feature_parser.add_argument('--opt', dest='read_slt', action='store_false', help="Use OPT representation")
    parser.set_defaults(read_slt=True)

    feature_parser = parser.add_mutually_exclusive_group(required=False)
    feature_parser.add_argument('--ifrp', dest='frp', action='store_true', help="Ignore full relative path")
    feature_parser.add_argument('--ufrp', dest='frp', action='store_false', help="Use full relative path")
    parser.set_defaults(frp=True)

    feature_parser = parser.add_mutually_exclusive_group(required=False)
    feature_parser.add_argument('--ta', dest='ta', action='store_true', help="Tokenize the all values")
    feature_parser.add_argument('--nta', dest='ta', action='store_false', help="Tokenize the all values")
    parser.set_defaults(ta=False)

    feature_parser = parser.add_mutually_exclusive_group(required=False)
    feature_parser.add_argument('--tn', dest='tn', action='store_true', help="Tokenize the number values")
    feature_parser.add_argument('--ntn', dest='tn', action='store_false', help="Not tokenize the number values")
    parser.set_defaults(tn=False)

    feature_parser = parser.add_mutually_exclusive_group(required=False)
    feature_parser.add_argument('--wiki', dest='wiki', action='store_true', help="Use NTCIR-12 Dataset")
    feature_parser.add_argument('--mse', dest='wiki', action='store_false', help="Use ARQMath Dataset")
    parser.set_defaults(wiki=True)


    args = vars(parser.parse_args())

    train_model = args['train']
    do_retrieval = args['retrieval']
    dataset_file_path = args['ds']
    config_id = args['cid']
    is_wiki = args['wiki']
    read_slt = bool(args['read_slt'])
    encoder_file_path = args['em']
    model_file_path = args['mp']
    res_file = "Retrieval_Results/" + args['rf']
    run_id = args['ri']
    ignore_full_relative_path = args['frp']
    tokenize_all = args['ta']
    tokenize_number = args['tn']
    queries_directory_path = args['qd']
    embedding_type = TupleTokenizationMode(args['et'])
    map_file_path = "Embedding_Preprocessing/" + str(encoder_file_path)
    config_file_path = "Configuration/config/config_" + str(config_id)
    model_file_path = "Saved_Models/" + model_file_path
    # print(args)
    # input("wait")
    system = TangentCFTBackEnd(config_file=config_file_path, path_data_set=dataset_file_path, is_wiki=is_wiki,
                               read_slt=read_slt, queries_directory_path=queries_directory_path)
    if train_model:
        dictionary_formula_tuples_collection = system.train_model(
            map_file_path=map_file_path,
            model_file_path=model_file_path,
            embedding_type=embedding_type, ignore_full_relative_path=ignore_full_relative_path,
            tokenize_all=tokenize_all,
            tokenize_number=tokenize_number
        )
        if do_retrieval:
            retrieval_result = system.retrieval(dictionary_formula_tuples_collection,
                                                embedding_type, ignore_full_relative_path, tokenize_all,
                                                tokenize_number
                                                )
            system.create_result_file(retrieval_result, res_file, run_id)
    else:

        dictionary_formula_tuples_collection = system.load_model(
            map_file_path=map_file_path,
            model_file_path=model_file_path,
            embedding_type=embedding_type, ignore_full_relative_path=ignore_full_relative_path,
            tokenize_all=tokenize_all,
            tokenize_number=tokenize_number
        )
        if do_retrieval:
            retrieval_result = system.retrieval(dictionary_formula_tuples_collection,
                                                embedding_type, ignore_full_relative_path, tokenize_all,
                                                tokenize_number
                                                )
            system.create_result_file(retrieval_result, res_file, run_id)


if __name__ == "__main__":
    main()
