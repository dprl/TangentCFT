#!/bin/bash
python3 tangent_cft_front_end.py -ds "NTCIR12_MathIR_WikiCorpus_v2.1.0/MathTagArticles" -cid 1  -em slt_encoder.tsv --mp slt_model --rf slt_ret.tsv --qd "TestQueries" --ri 1 --tn
python3 tangent_cft_front_end.py -ds "NTCIR12_MathIR_WikiCorpus_v2.1.0/MathTagArticles" -cid 2  -em opt_encoder.tsv --mp opt_model --rf opt_ret.tsv --qd "TestQueries" --ri 2 --opt
python3 tangent_cft_front_end.py -ds "NTCIR12_MathIR_WikiCorpus_v2.1.0/MathTagArticles" -cid 3  -em slt_type_encoder.tsv --mp slt_type_model --rf slt_type_ret.tsv --qd "TestQueries" --et 2 --ri 3
python3 tangent_cft_combine_results.py